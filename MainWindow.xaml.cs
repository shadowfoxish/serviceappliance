﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Service_Appliance
{

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		
		private object SyncRoot = new object();
		private MainWindowViewModel vm;

		static readonly CancellationTokenSource s_cts = new CancellationTokenSource();

		public MainWindow()
		{
			InitializeComponent();
			vm = new MainWindowViewModel();
			base.DataContext = vm;
			Task.Run(() => RefreshServices(vm.ServiceData));
			Task.Run(() => Status.Tickler(s_cts.Token));
		}
		protected override void OnClosing(CancelEventArgs e)
		{
			s_cts.Cancel();
			base.OnClosing(e);
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var data = (base.DataContext as MainWindowViewModel).ServiceData;
			Task.Run(() => RefreshServices(data));
		}

		private void RefreshServices(List<Service> serviceData)
		{
			this.vm.TotalNumServices = serviceData.Count();
			this.vm.NumServicesChecked = 0;
			this.vm.Progress = 0;

			foreach (var s in serviceData)
			{
				s.ServiceState = null;
			}

			Parallel.ForEach(serviceData, s => Refresh(s));
		}

		private void StartService_Click(object sender, RoutedEventArgs e)
		{
			Service s = ((FrameworkElement)sender).DataContext as Service;
			Task.Run(() => StartService(s));
		}

		private void StopService_Click(object sender, RoutedEventArgs e)
		{
			Service s = ((FrameworkElement)sender).DataContext as Service;
			Task.Run(() => StopService(s));
		}

		private void StopService(Service s)
		{
			try
			{
				ServiceController c = new ServiceController(s.ServiceName, s.MachineName);
				s.ServiceState = c.Status;
				if(c.CanStop && c.Status == ServiceControllerStatus.Running)
				{
					c.Stop();
					s.ServiceState = ServiceControllerStatus.StopPending;
					c.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(20));
					s.ServiceState = c.Status;
					s.Message = "OK";
				}
				else
				{
					s.Message = "Not allowed";
				}
			}
			catch (Exception ex)
			{
				s.Message = "ERROR - " + ex.Message;
			}
		}
		private void StartService(Service s)
		{
			try
			{
				ServiceController c = new ServiceController(s.ServiceName, s.MachineName);
				s.ServiceState = c.Status;
				if (c.Status == ServiceControllerStatus.Stopped)
				{
					c.Start();
					s.ServiceState = ServiceControllerStatus.StartPending;
					c.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(20));
					s.ServiceState = c.Status;
					s.Message = "OK";
				}
				else
				{
					s.Message = "Not allowed";
				}
			}
			catch (Exception ex)
			{
				s.Message = "ERROR - " + ex.Message;
			}
		}
		private void Refresh(Service s)
		{
			try
			{
				ServiceController c = new ServiceController(s.ServiceName, s.MachineName);
				s.ServiceState = c.Status;
				s.Message = "OK";
				lock (SyncRoot)
				{
					this.vm.NumServicesChecked++;
					this.vm.Progress = (int)((decimal)this.vm.NumServicesChecked / this.vm.TotalNumServices * 100);
				}
			}
			catch (Exception ex)
			{
				s.ServiceState = null;
				s.Message = "ERROR - " + ex.Message;
			}
		}
	}
}
