﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Service_Appliance
{
	[Serializable]
	public class ServiceConfig
	{
		public static string GetConfigPath()
		{
			return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ServiceApplianceConfig.xml");
		}

		public static ServiceConfig LoadConfig(string path)
		{
			if (File.Exists(path))
			{
				XmlSerializer ser = new XmlSerializer(typeof(ServiceConfig));
				using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
				using (StreamReader sr = new StreamReader(fs))
				{
					return (ServiceConfig)ser.Deserialize(sr);
				}
			}
			else
			{
				ServiceConfig c = new ServiceConfig()
				{
					Config = new List<ServiceConfigData>()
					{
						new ServiceConfigData()
						{
							Category = "CAT",
							Environment = "ENV",
							MachineName = "MACHINE",
							ServiceName = "SVC_Name",
						}
					}
				};
				SaveConfig(c, path);
				return c;
			}
		}

		public static void SaveConfig(ServiceConfig config, string path)
		{
			XmlSerializer ser = new XmlSerializer(typeof(ServiceConfig));
			using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
			using (StreamWriter sw = new StreamWriter(fs))
			{
				ser.Serialize(sw, config);
			}
		}

		public List<ServiceConfigData> Config = new List<ServiceConfigData>();

		public ServiceConfig()
		{

		}

		[Serializable]
		public class ServiceConfigData
		{
			[XmlAttribute]
			public string Environment { get; set; }
			[XmlAttribute]
			public string MachineName { get; set; }
			[XmlAttribute]
			public string Category { get; set; }
			[XmlAttribute]
			public string ServiceName { get; set; }
		}
	}
}
