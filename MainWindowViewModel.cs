﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Service_Appliance
{
	public class MainWindowViewModel : INotifyPropertyChanged
	{
		public List<Service> ServiceData;

		public ICollectionView ServiceDataView { get; private set; }
		public event PropertyChangedEventHandler PropertyChanged;

		protected void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		

		private int totalNumServices = 0;
		public int TotalNumServices
		{
			get
			{
				return totalNumServices;
			}
			set
			{
				this.totalNumServices = value;
				NotifyPropertyChanged(nameof(TotalNumServices));
			}
		}

		private int numServicesChecked = 0;
		public int NumServicesChecked
		{
			get
			{
				return numServicesChecked;
			}
			set
			{
				this.numServicesChecked = value;
				NotifyPropertyChanged(nameof(TotalNumServices));
			}
		}

		private int progress = 35;
		public int Progress
		{
			get
			{
				return progress;
			}
			set
			{
				this.progress = value;
				NotifyPropertyChanged(nameof(Progress));
			}
		}

		public MainWindowViewModel()
		{
			ServiceConfig cfg = ServiceConfig.LoadConfig(ServiceConfig.GetConfigPath());
			this.ServiceData = cfg.Config.Select(c => new Service()
			{
				Environment = c.Environment,
				MachineName = c.MachineName,
				ServiceName = c.ServiceName,
				Category = c.Category,
			}).ToList();
			this.ServiceDataView = CollectionViewSource.GetDefaultView(this.ServiceData);
			this.ServiceDataView.SortDescriptions.Add(new SortDescription(nameof(Service.Environment), ListSortDirection.Ascending));
			this.ServiceDataView.SortDescriptions.Add(new SortDescription(nameof(Service.Category), ListSortDirection.Ascending));
		}
	}
}
