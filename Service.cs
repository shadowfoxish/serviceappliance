﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Service_Appliance
{
	public class Service : INotifyPropertyChanged
	{
		private string environment;
		private string machineName;
		private ServiceControllerStatus? serviceState;
		private string serviceName;
		private string message;
		private string category;
		private bool serviceRunning = false;

		public string Environment
		{
			get
			{
				return environment;
			}
			set
			{
				environment = value;
				NotifyPropertyChanged(nameof(Environment));
			}
		}
		public string MachineName
		{
			get
			{
				return machineName;
			}
			set
			{
				machineName = value;
				NotifyPropertyChanged(nameof(MachineName));
			}
		}
		public ServiceControllerStatus? ServiceState
		{
			get
			{
				return serviceState;
			}
			set
			{
				if (value == ServiceControllerStatus.Running)
				{
					serviceRunning = true;
				}
				else
				{
					serviceRunning = false;
				}
				serviceState = value;
				NotifyPropertyChanged(nameof(ServiceState));
			}
		}
		public string ServiceName
		{
			get
			{
				return serviceName;
			}
			set
			{
				serviceName = value;
				NotifyPropertyChanged(nameof(ServiceName));
			}
		}
		public string Message
		{
			get
			{
				return message;
			}
			set
			{
				message = value;
				NotifyPropertyChanged(nameof(Message));
			}
		}

		public string Category
		{
			get
			{
				return category;
			}
			set
			{
				category = value;
				NotifyPropertyChanged(nameof(Category));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
