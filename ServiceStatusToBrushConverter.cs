﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Service_Appliance
{
	public class ServiceStatusToBrushConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			ServiceControllerStatus? status = value as ServiceControllerStatus?;
			switch (status)
			{
				case ServiceControllerStatus.Running:
					return Brushes.LightGreen;
				case ServiceControllerStatus.Stopped:
					return Brushes.PaleVioletRed;
				default:
					return Brushes.Cyan;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
